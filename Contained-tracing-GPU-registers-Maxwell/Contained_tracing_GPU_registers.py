#The MIT License
#Copyright (c) 2018 Jesus P. Espinoza
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#Execute in cuda-gdb
#Key sample: 0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80, 0x90, 0xA0, 0xB0, 0xC0, 0xD0, 0xE0, 0xF0

def set_kernel_break(): return gdb.execute("set cuda break_on_launch application",to_string=True);
set_kernel_break()
def run_kernel_break(): return gdb.execute("run",to_string=True);
print(run_kernel_break())
number_of_steps=1000000
def assembly_nexti(): return gdb.execute("nexti",to_string=True);
for i in range(number_of_steps):
  assembly_nexti()
  def read_registers(): return gdb.execute("info registers",to_string=True);
  def cuda_read_global_memory(addr): return gdb.parse_and_eval("* (@global unsigned int *) %s" % (addr));
  addresses = []
  info_registers = read_registers()
  info_registers = info_registers.split()
  for value in info_registers:
    if "0x" in value and "0x0" != value:
      addresses.append('{:08x}'.format(int(value,16)))
  for hex_value in addresses:
    for hex_value2 in addresses:
      address_possibility = str("0x"+str(hex_value)+str(hex_value2))
      try:
        memory_value = cuda_read_global_memory(address_possibility)
        str_memory_value = str('{:08x}'.format(int(memory_value))) 
        if str_memory_value == "00102030":
          print("[+] " + str(i))
          print('{:08x}'.format(int(memory_value))+" - "+address_possibility)
          address_possibility_1 = int(address_possibility,16) + 0x8
          address_possibility_1 = "0x"+str('{:08x}'.format(int(address_possibility_1)))
          memory_value_1 = cuda_read_global_memory(address_possibility_1)
          print('{:08x}'.format(int(memory_value_1))+" - "+address_possibility_1)
          address_possibility_1 = int(address_possibility,16) + 0x10
          address_possibility_1 = "0x"+str('{:08x}'.format(int(address_possibility_1)))
          memory_value_1 = cuda_read_global_memory(address_possibility_1)
          print('{:08x}'.format(int(memory_value_1))+" - "+address_possibility_1)
          address_possibility_1 = int(address_possibility,16) + 0x18
          address_possibility_1 = "0x"+str('{:08x}'.format(int(address_possibility_1)))
          memory_value_1 = cuda_read_global_memory(address_possibility_1)
          print('{:08x}'.format(int(memory_value_1))+" - "+address_possibility_1)
      except:
        pass
