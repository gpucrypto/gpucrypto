/*
The MIT License
Copyright (c) 2018 Jesus P. Espinoza
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include "aes.h"
#include <stdint.h>

#if defined(_MSC_VER) && !defined(OPENSSL_SYS_WINCE)
# define SWAP(x) (_lrotl(x, 8) & 0x00ff00ff | _lrotr(x, 8) & 0xff00ff00)
# define GETU32(p) SWAP(*((u32 *)(p)))
# define PUTU32(ct, st) { *((u32 *)(ct)) = SWAP((st)); }
#else
# define GETU32(pt) (((u32)(pt)[0] << 24) ^ ((u32)(pt)[1] << 16) ^ ((u32)(pt)[2] <<  8) ^ ((u32)(pt)[3]))
# define PUTU32(ct, st) { (ct)[0] = (u8)((st) >> 24); (ct)[1] = (u8)((st) >> 16); (ct)[2] = (u8)((st) >>  8); (ct)[3] = (u8)(st); }
#endif

int run_threads = 32;

typedef unsigned long u32;
unsigned char key[16] = {
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00
};
struct cipher_timing
{		 
	char cipher[32*16];
	uint32_t timing;
};

struct cache_line_count
{
	int index; 
  short int Te4_index[32];
  uint32_t timing;
  int max_conflicts;
};

unsigned long long block_samples = 2097152;
int j_byte[1] = {0};
unsigned char key_byte = key[j_byte[0]]; 
const int THREAD_COUNT = 1024;
const int BLOCK_COUNT = 64;
const int TOTAL_COUNT = THREAD_COUNT * BLOCK_COUNT;


struct cipher_timing data_block[TOTAL_COUNT];
AES_KEY *expanded = (AES_KEY *) malloc(sizeof(AES_KEY) * 32);
int cache_line_requests[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
int cache_line_count=0;
__device__ u32 Te4_d[256];


unsigned char key_enumeration[256] = {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F,0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2B,0x2C,0x2D,0x2E,0x2F,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3C,0x3D,0x3E,0x3F,0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4A,0x4B,0x4C,0x4D,0x4E,0x4F,0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5A,0x5B,0x5C,0x5D,0x5E,0x5F,0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6A,0x6B,0x6C,0x6D,0x6E,0x6F,0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7A,0x7B,0x7C,0x7D,0x7E,0x7F,0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8A,0x8B,0x8C,0x8D,0x8E,0x8F,0x90,0x91,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0x9A,0x9B,0x9C,0x9D,0x9E,0x9F,0xA0,0xA1,0xA2,0xA3,0xA4,0xA5,0xA6,0xA7,0xA8,0xA9,0xAA,0xAB,0xAC,0xAD,0xAE,0xAF,0xB0,0xB1,0xB2,0xB3,0xB4,0xB5,0xB6,0xB7,0xB8,0xB9,0xBA,0xBB,0xBC,0xBD,0xBE,0xBF,0xC0,0xC1,0xC2,0xC3,0xC4,0xC5,0xC6,0xC7,0xC8,0xC9,0xCA,0xCB,0xCC,0xCD,0xCE,0xCF,0xD0,0xD1,0xD2,0xD3,0xD4,0xD5,0xD6,0xD7,0xD8,0xD9,0xDA,0xDB,0xDC,0xDD,0xDE,0xDF,0xE0,0xE1,0xE2,0xE3,0xE4,0xE5,0xE6,0xE7,0xE8,0xE9,0xEA,0xEB,0xEC,0xED,0xEE,0xEF,0xF0,0xF1,0xF2,0xF3,0xF4,0xF5,0xF6,0xF7,0xF8,0xF9,0xFA,0xFB,0xFC,0xFD,0xFE,0xFF};

static const u32 Te4_h[256] = {
    0x63636363U, 0x7c7c7c7cU, 0x77777777U, 0x7b7b7b7bU,  
    0xf2f2f2f2U, 0x6b6b6b6bU, 0x6f6f6f6fU, 0xc5c5c5c5U,  
    0x30303030U, 0x01010101U, 0x67676767U, 0x2b2b2b2bU,  
    0xfefefefeU, 0xd7d7d7d7U, 0xababababU, 0x76767676U,  
    0xcacacacaU, 0x82828282U, 0xc9c9c9c9U, 0x7d7d7d7dU,  
    0xfafafafaU, 0x59595959U, 0x47474747U, 0xf0f0f0f0U,  
    0xadadadadU, 0xd4d4d4d4U, 0xa2a2a2a2U, 0xafafafafU,  
    0x9c9c9c9cU, 0xa4a4a4a4U, 0x72727272U, 0xc0c0c0c0U,  
    0xb7b7b7b7U, 0xfdfdfdfdU, 0x93939393U, 0x26262626U,  
    0x36363636U, 0x3f3f3f3fU, 0xf7f7f7f7U, 0xccccccccU,  
    0x34343434U, 0xa5a5a5a5U, 0xe5e5e5e5U, 0xf1f1f1f1U,  
    0x71717171U, 0xd8d8d8d8U, 0x31313131U, 0x15151515U,  
    0x04040404U, 0xc7c7c7c7U, 0x23232323U, 0xc3c3c3c3U,  
    0x18181818U, 0x96969696U, 0x05050505U, 0x9a9a9a9aU,  
    0x07070707U, 0x12121212U, 0x80808080U, 0xe2e2e2e2U,  
    0xebebebebU, 0x27272727U, 0xb2b2b2b2U, 0x75757575U,  
    0x09090909U, 0x83838383U, 0x2c2c2c2cU, 0x1a1a1a1aU,  
    0x1b1b1b1bU, 0x6e6e6e6eU, 0x5a5a5a5aU, 0xa0a0a0a0U,  
    0x52525252U, 0x3b3b3b3bU, 0xd6d6d6d6U, 0xb3b3b3b3U,  
    0x29292929U, 0xe3e3e3e3U, 0x2f2f2f2fU, 0x84848484U,  
    0x53535353U, 0xd1d1d1d1U, 0x00000000U, 0xededededU,  
    0x20202020U, 0xfcfcfcfcU, 0xb1b1b1b1U, 0x5b5b5b5bU,  
    0x6a6a6a6aU, 0xcbcbcbcbU, 0xbebebebeU, 0x39393939U,  
    0x4a4a4a4aU, 0x4c4c4c4cU, 0x58585858U, 0xcfcfcfcfU,  
    0xd0d0d0d0U, 0xefefefefU, 0xaaaaaaaaU, 0xfbfbfbfbU,  
    0x43434343U, 0x4d4d4d4dU, 0x33333333U, 0x85858585U,  
    0x45454545U, 0xf9f9f9f9U, 0x02020202U, 0x7f7f7f7fU,  
    0x50505050U, 0x3c3c3c3cU, 0x9f9f9f9fU, 0xa8a8a8a8U,  
    0x51515151U, 0xa3a3a3a3U, 0x40404040U, 0x8f8f8f8fU,  
    0x92929292U, 0x9d9d9d9dU, 0x38383838U, 0xf5f5f5f5U,  
    0xbcbcbcbcU, 0xb6b6b6b6U, 0xdadadadaU, 0x21212121U,  
    0x10101010U, 0xffffffffU, 0xf3f3f3f3U, 0xd2d2d2d2U,  
    0xcdcdcdcdU, 0x0c0c0c0cU, 0x13131313U, 0xececececU,  
    0x5f5f5f5fU, 0x97979797U, 0x44444444U, 0x17171717U,  
    0xc4c4c4c4U, 0xa7a7a7a7U, 0x7e7e7e7eU, 0x3d3d3d3dU,  
    0x64646464U, 0x5d5d5d5dU, 0x19191919U, 0x73737373U,  
    0x60606060U, 0x81818181U, 0x4f4f4f4fU, 0xdcdcdcdcU,  
    0x22222222U, 0x2a2a2a2aU, 0x90909090U, 0x88888888U,  
    0x46464646U, 0xeeeeeeeeU, 0xb8b8b8b8U, 0x14141414U,  
    0xdedededeU, 0x5e5e5e5eU, 0x0b0b0b0bU, 0xdbdbdbdbU,  
    0xe0e0e0e0U, 0x32323232U, 0x3a3a3a3aU, 0x0a0a0a0aU,  
    0x49494949U, 0x06060606U, 0x24242424U, 0x5c5c5c5cU,  
    0xc2c2c2c2U, 0xd3d3d3d3U, 0xacacacacU, 0x62626262U,  
    0x91919191U, 0x95959595U, 0xe4e4e4e4U, 0x79797979U,  
    0xe7e7e7e7U, 0xc8c8c8c8U, 0x37373737U, 0x6d6d6d6dU,  
    0x8d8d8d8dU, 0xd5d5d5d5U, 0x4e4e4e4eU, 0xa9a9a9a9U,  
    0x6c6c6c6cU, 0x56565656U, 0xf4f4f4f4U, 0xeaeaeaeaU,  
    0x65656565U, 0x7a7a7a7aU, 0xaeaeaeaeU, 0x08080808U,  
    0xbabababaU, 0x78787878U, 0x25252525U, 0x2e2e2e2eU,  
    0x1c1c1c1cU, 0xa6a6a6a6U, 0xb4b4b4b4U, 0xc6c6c6c6U,  
    0xe8e8e8e8U, 0xddddddddU, 0x74747474U, 0x1f1f1f1fU,  
    0x4b4b4b4bU, 0xbdbdbdbdU, 0x8b8b8b8bU, 0x8a8a8a8aU,  
    0x70707070U, 0x3e3e3e3eU, 0xb5b5b5b5U, 0x66666666U,  
    0x48484848U, 0x03030303U, 0xf6f6f6f6U, 0x0e0e0e0eU,  
    0x61616161U, 0x35353535U, 0x57575757U, 0xb9b9b9b9U,  
    0x86868686U, 0xc1c1c1c1U, 0x1d1d1d1dU, 0x9e9e9e9eU,  
    0xe1e1e1e1U, 0xf8f8f8f8U, 0x98989898U, 0x11111111U,  
    0x69696969U, 0xd9d9d9d9U, 0x8e8e8e8eU, 0x94949494U,  
    0x9b9b9b9bU, 0x1e1e1e1eU, 0x87878787U, 0xe9e9e9e9U,  
    0xcecececeU, 0x55555555U, 0x28282828U, 0xdfdfdfdfU,  
    0x8c8c8c8cU, 0xa1a1a1a1U, 0x89898989U, 0x0d0d0d0dU,  
    0xbfbfbfbfU, 0xe6e6e6e6U, 0x42424242U, 0x68686868U,  
    0x41414141U, 0x99999999U, 0x2d2d2d2dU, 0x0f0f0f0fU,  
    0xb0b0b0b0U, 0x54545454U, 0xbbbbbbbbU, 0x16161616U,  
};


static const u32 rcon[] = {
                0x01000000, 0x02000000, 0x04000000, 0x08000000,
                0x10000000, 0x20000000, 0x40000000, 0x80000000,
                0x1B000000, 0x36000000, /* for 128-bit blocks, Rijndael never uses more than 10 rcon values */
};


int AES_set_encrypt_key(const unsigned char *userKey, const int bits,
                        AES_KEY *key) {

        u32 *rk;
        int i = 0;
        u32 temp;

        if (!userKey || !key)
                return -1;
        if (bits != 128 && bits != 192 && bits != 256)
                return -2;

        rk = key->rd_key;

        if (bits==128)
                key->rounds = 10;
        else if (bits==192)
                key->rounds = 12;
        else
                key->rounds = 14;

        rk[0] = GETU32(userKey     );
        rk[1] = GETU32(userKey +  4);
        rk[2] = GETU32(userKey +  8);
        rk[3] = GETU32(userKey + 12);
        if (bits == 128) {
                for (;;) {
                        temp  = rk[3];
                        rk[4] = rk[0] ^
                                (Te4_h[(temp >> 16) & 0xff] & 0xff000000) ^
                                (Te4_h[(temp >>  8) & 0xff] & 0x00ff0000) ^
                                (Te4_h[(temp      ) & 0xff] & 0x0000ff00) ^
                                (Te4_h[(temp >> 24)       ] & 0x000000ff) ^
                                rcon[i];
                        rk[5] = rk[1] ^ rk[4];
                        rk[6] = rk[2] ^ rk[5];
                        rk[7] = rk[3] ^ rk[6];
                        if (++i == 10) {
                                return 0;
                        }
                        rk += 4;
                }
        }
        rk[4] = GETU32(userKey + 16);
        rk[5] = GETU32(userKey + 20);
        if (bits == 192) {
                for (;;) {
                        temp = rk[ 5];
                        rk[ 6] = rk[ 0] ^
                                (Te4_h[(temp >> 16) & 0xff] & 0xff000000) ^
                                (Te4_h[(temp >>  8) & 0xff] & 0x00ff0000) ^
                                (Te4_h[(temp      ) & 0xff] & 0x0000ff00) ^
                                (Te4_h[(temp >> 24)       ] & 0x000000ff) ^
                                rcon[i];
                        rk[ 7] = rk[ 1] ^ rk[ 6];
                        rk[ 8] = rk[ 2] ^ rk[ 7];
                        rk[ 9] = rk[ 3] ^ rk[ 8];
                        if (++i == 8) {
                                return 0;
                        }
                        rk[10] = rk[ 4] ^ rk[ 9];
                        rk[11] = rk[ 5] ^ rk[10];
                        rk += 6;
                }
        }
        rk[6] = GETU32(userKey + 24);
        rk[7] = GETU32(userKey + 28);
        if (bits == 256) {
                for (;;) {
                        temp = rk[ 7];
                        rk[ 8] = rk[ 0] ^
                                (Te4_h[(temp >> 16) & 0xff] & 0xff000000) ^
                                (Te4_h[(temp >>  8) & 0xff] & 0x00ff0000) ^
                                (Te4_h[(temp      ) & 0xff] & 0x0000ff00) ^
                                (Te4_h[(temp >> 24)       ] & 0x000000ff) ^
                                rcon[i];
                        rk[ 9] = rk[ 1] ^ rk[ 8];
                        rk[10] = rk[ 2] ^ rk[ 9];
                        rk[11] = rk[ 3] ^ rk[10];
                        if (++i == 7) {
                                return 0;
                        }
                        temp = rk[11];
                        rk[12] = rk[ 4] ^
                                (Te4_h[(temp >> 24)       ] & 0xff000000) ^
                                (Te4_h[(temp >> 16) & 0xff] & 0x00ff0000) ^
                                (Te4_h[(temp >>  8) & 0xff] & 0x0000ff00) ^
                                (Te4_h[(temp      ) & 0xff] & 0x000000ff);
                        rk[13] = rk[ 5] ^ rk[12];
                        rk[14] = rk[ 6] ^ rk[13];
                        rk[15] = rk[ 7] ^ rk[14];

                        rk += 8;
                }
        }
        return 0;
}

void last_key(){

unsigned long long i = 0;
    
    for (i = 0; i < 32; i++){         
        AES_set_encrypt_key(key, 128, expanded + i);
    }
}
 
__global__ void warp_Te4_inverse_kernel(unsigned char *d_key_possibility, cipher_timing* d_data_block, struct cache_line_count* d_analysis_array, int* d_j_byte){        
        unsigned char d_cipher_byte = 0x00;
 	      int cache_line_size = 16;         
	      unsigned char lookup_hex = 0x00; 
        int cache_line =0; 
        register int d_cache_line_requests[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        unsigned long tId = (threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y)+(blockIdx.x + blockIdx.y * gridDim.x) *(blockDim.x * blockDim.y * blockDim.z);
  
        for(int k=0; k<32;k++){
          for(int j=0; j<16; j++){
             if(j==d_j_byte[0]){                
                d_cipher_byte = d_data_block[tId].cipher[j+ k*16] & 0xff;
                lookup_hex = d_cipher_byte ^ d_key_possibility[0];
                cache_line = 0;
	        for(int i=0; i<256; i++){
		if((Te4_d[i] & 0x000000ff) == lookup_hex){
			d_analysis_array[tId].Te4_index[k]=i;
                        }
	     }
       } 
       } 
       }     
       d_analysis_array[tId].index = tId;
       d_analysis_array[tId].timing = d_data_block[tId].timing;   
       for (int x=0; x<32; x++){
       int max_conflicts =0;
       int conflicts_queue[32] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
       int queue_pointer = 0;
	     for(int j=0;j<32;j++){
	         if(  ((d_analysis_array[tId].Te4_index[x]%32) == (d_analysis_array[tId].Te4_index[j]%32)) && (d_analysis_array[tId].Te4_index[x] != d_analysis_array[tId].Te4_index[j]) && max_conflicts==0)  {
		       max_conflicts++;
                       conflicts_queue[queue_pointer] = d_analysis_array[tId].Te4_index[j];
                       queue_pointer++;
		   }else if(  ((d_analysis_array[tId].Te4_index[x]%32) == (d_analysis_array[tId].Te4_index[j]%32)) && (d_analysis_array[tId].Te4_index[x] != d_analysis_array[tId].Te4_index[j]) && max_conflicts>0)  {
                    bool existing_conflict=false; 
                    for(int ptr = 0; ptr<queue_pointer;ptr++){  
		         if(d_analysis_array[tId].Te4_index[j]==conflicts_queue[ptr]){existing_conflict=true;}	
		   }

		    if(!existing_conflict){
			 max_conflicts++;
			 conflicts_queue[queue_pointer] = d_analysis_array[tId].Te4_index[j];
                        queue_pointer++;
		   }
       }
	     }
       d_analysis_array[tId].max_conflicts = max_conflicts;
       }   
}

int main(int argc, char **argv){
	int counter;
	FILE *ptr_cipherfile;
        char out_file_name[100];
        char analysis_file_name[100];
        char analysis_file_name_2[100];
        char analysis_file_name_4[100];
        unsigned char *d_cipher_byte;
	      sprintf(out_file_name, "out_%llu.bin",block_samples);
        AES_KEY *d_expanded;
        int d_cache_line_requests;
        int d_cache_line_count;    
        struct cipher_timing *d_data_block; 
        cudaMemcpyToSymbol(Te4_d, &Te4_h, 256*sizeof(u32),0,cudaMemcpyHostToDevice);  
        struct cache_line_count analysis_array[TOTAL_COUNT];
        struct cache_line_count *d_analysis_array;
        int *d_j_byte;
        unsigned char key_possibility[1];
        unsigned char *d_key_possibility;
	      ptr_cipherfile = fopen(out_file_name,"rb");
        
        for(int byte_count=0; byte_count<=15; byte_count++){  
           j_byte[0]=byte_count;
        
	      for(int x=0; x<TOTAL_COUNT; x++){
           memset(analysis_array[x].Te4_index,0,16);
           analysis_array[x].index = 0;
           analysis_array[x].timing = 0;
	   	  }
    
        cudaMalloc((void**)&d_j_byte, sizeof(int));
        cudaMemcpy(d_j_byte, j_byte, sizeof(int), cudaMemcpyHostToDevice);

        for(int p_index=0; p_index <= 255; p_index++){  
        key_possibility[0] = key_enumeration[p_index];
        cudaMalloc((void**)&d_key_possibility, sizeof(unsigned char));
        cudaMemcpy(d_key_possibility, key_possibility, sizeof(unsigned char), cudaMemcpyHostToDevice);

	      FILE *ptr_outputanalysis_2;
        sprintf(analysis_file_name_2, "a_%d_%llu_%02x_2.csv",block_samples,j_byte[0],key_enumeration[p_index]);
        ptr_outputanalysis_2 = fopen(analysis_file_name_2,"w");
	 
	      FILE *ptr_outputanalysis_4;
        sprintf(analysis_file_name_4, "a_%d_%llu_%02x_4.csv",block_samples,j_byte[0],key_enumeration[p_index]);
        ptr_outputanalysis_4 = fopen(analysis_file_name_4,"w");
     
	      if (!ptr_cipherfile || !ptr_outputanalysis_2 || !ptr_outputanalysis_4)
       	{
		    printf("[-] Unable to find or open file.");
		    return 1;
	      }
         
        fprintf(ptr_outputanalysis_2,"timing\n");
        fprintf(ptr_outputanalysis_4,"timing\n");
        rewind(ptr_cipherfile); 
        
        for (counter = 0; counter < (block_samples / TOTAL_COUNT); counter++)
	      {
		    fread(&data_block,sizeof(struct cipher_timing),TOTAL_COUNT,ptr_cipherfile);  
                                		
                cudaMalloc((void**)&d_data_block, sizeof(struct cipher_timing)*TOTAL_COUNT);  
                cudaMemcpy(d_data_block, data_block, sizeof(struct cipher_timing)*TOTAL_COUNT, cudaMemcpyHostToDevice);  
                
                cudaMalloc((void**)&d_analysis_array, sizeof(struct cache_line_count)*TOTAL_COUNT);  
                cudaMemcpy(d_analysis_array, analysis_array, sizeof(struct cache_line_count)*TOTAL_COUNT, cudaMemcpyHostToDevice);  

                warp_Te4_inverse_kernel<<<BLOCK_COUNT,THREAD_COUNT>>>(d_key_possibility, d_data_block, d_analysis_array, d_j_byte);  
                cudaDeviceSynchronize();               
                cudaMemcpy(analysis_array, d_analysis_array, sizeof(struct cache_line_count)*TOTAL_COUNT, cudaMemcpyDeviceToHost);  
        
                for(int i=0; i<TOTAL_COUNT; i++){
                      char Te4_index_str[1000];
                      int index_str = 0;
                      for (int j=0; j<32; j++){
                           index_str += sprintf(&Te4_index_str[index_str],"%d,",analysis_array[i].Te4_index[j]);
                      }  
                      if(analysis_array[i].max_conflicts==1){
  	                     fprintf(ptr_outputanalysis_2,"%d\n",analysis_array[i].timing);}
                      if(analysis_array[i].max_conflicts==3){
  	                     fprintf(ptr_outputanalysis_4,"%d\n",analysis_array[i].timing);}			
                }       
                cudaFree(d_analysis_array);
                cudaFree(d_data_block);
	    
           for(int x=0; x<TOTAL_COUNT; x++){
	    
           memset(analysis_array[x].Te4_index,0,16);
           analysis_array[x].index = 0;
           analysis_array[x].timing = 0;
	   	  }   		
	      } 	
        fclose(ptr_outputanalysis_2);
        fclose(ptr_outputanalysis_4);
        }  
        } 
	return 0;
}
