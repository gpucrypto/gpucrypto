The MIT License
Copyright (c) 2018 Jesus P. Espinoza
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

[+] Notes:

This repository contains the source code of an academic experiment developed by Jesus P. Espinoza and supervised by Professor Jason M. Pittman D.Sc., Professor Daniel R. Ford D.Sc, and Professor Mary M. Chantre Ed.D. at Capitol Technology University for contained timing, access, and tracing attacks for AES key recovery and extraction in GPUs with Maxwell architecture (e.g., GTX 980 and Tesla M60).

GPU-assisted AES for CUDA was supplied by the Electrical and Computer Engineering Department of Northeastern University. GPU-assisted AES for CUDA can be found at https://tescase.coe.neu.edu

References used in this development:

Espinoza, J. P. (2018). Dynamic malware analysis of GPU-assisted cryptoviruses using contained AES side-channels.
Jiang, Z. H., Fei, Y., & Kaeli, D. (2016, March). A complete key recovery timing attack on a GPU. 2016 IEEE International Symposium on High-Performance Computer Architecture, Barcelona, Spain, 394–405. doi:10.1109/HPCA.2016.7446081
Jiang, Z. H., Fei, Y., & Kaeli, D. (2017). A novel side-channel timing attack on GPUs. Proceedings of the on Great Lakes Symposium on VLSI 2017 (pp. 167-172). Banff, Alberta, Canada: ACM. doi:10.1145/3060403.3060462

